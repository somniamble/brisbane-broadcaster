# brisbane-broadcaster
A final project for an art class that I collaborated with a friend on. Text message interface to Twilio, which would forward on to this server, which would respond with the next message in a series of messages.

Built using Flask and Twilio's python library
