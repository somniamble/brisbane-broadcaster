#! /bin/env python
import json
from twilio.rest import TwilioRestClient
import twilio.twiml
from flask import Flask, request, redirect
app = Flask(__name__)


with open('content/belinda.json', 'r', encoding='utf-8') as infile:
    CONTENT = json.load(infile)

USERS = dict()

OFFENDERS = set()


def message(id_):
    return CONTENT[id_ - 1]['text']

def route(choice):
    return CONTENT[0]['children'].get(choice.lower())

def body():
    return request.values.get('Body')

@app.route("/", methods=['GET', 'POST'])
def respond():
    
    num = request.values.get('From', None)

    print(num,':', request.values.get('Body'))

    resp = twilio.twiml.Response()

    if num in OFFENDERS:
        resp.message("...")
    elif body().lower() == 'bryn':
        resp.message("Haven't you done enough?")
        OFFENDERS.add(num)
    elif num in USERS:
        print(USERS[num])
        if USERS[num] == 1:
            next_id = route(request.values.get('Body'))
            if next_id is None:
                resp.message("That's not a hallway...")
            else:
                USERS[num] = int(next_id)
                resp.message(message(int(next_id)))
        else:
            USERS[num] = int(CONTENT[USERS[num] - 1]['children'])
            resp.message(message(USERS[num]))
    else:
        USERS[num] = 1
        resp.message(message(1))

    return str(resp)
 
if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=False, port=80)
